const fs = require('fs');
const parser = require('papaparse');
const path = require('path');

const dataPath = path.join(__dirname, "src/data");
const functionPath = path.join(__dirname, "src/server/");
const outputPath = path.join(__dirname, "src/public/output");


const matchPerYearFinder = require(path.join(functionPath, "1-matches-per-year.cjs"));
const matchesWonPerTeamPerYear = require(path.join(functionPath, '2-matches-won-per-team-per-year.cjs'));
const extraRunConcede = require(path.join(functionPath, '3-extra-run-conceded.cjs'));
const top10EconomicalBowler = require(path.join(functionPath, '4-top-economic-bowler.cjs'));
const timesEachTeamWonTossAndMatch = require(path.join(functionPath, '5-times-each-team-won-toss-and-match.cjs'));
const playerHowWonHighestPlayerOfMatch = require(path.join(functionPath, "6-player-how-won-highest-player-of-match.cjs"));
const strikeRateOfBatsman = require(path.join(functionPath, "7-strikerate-of-batsman-for-eachseason.cjs"));
const highestPlayerDismisses = require(path.join(functionPath, "8-highest-playes-dismissed.cjs"));
const bestEconomyBowlerInSuperover = require(path.join(functionPath, '9-best-bowler-in-super-over.cjs'));


const outputFs = fs.readFileSync('./src/data/matches.csv',
    { encoding: 'utf8', flag: 'r' }
);

const matches = (parser.parse(outputFs.toString(),
    { header: true })).data;


const outputFsd = fs.readFileSync('./src/data/deliveries.csv',
    { encoding: 'utf8', flag: 'r' }
);

const deleveries = (parser.parse(outputFsd.toString(),
    { header: true })).data;



let result1 = matchPerYearFinder(matches);
fs.writeFileSync(path.join(outputPath, "1-matches-per-year.json"), JSON.stringify(result1));

let result2 = matchesWonPerTeamPerYear(matches);
fs.writeFileSync(path.join(outputPath, "2-matches-won-per-team-per-year.json"), JSON.stringify(result2));

let result3 = extraRunConcede(deleveries, matches);
fs.writeFileSync(path.join(outputPath, "3-extra-run-conceded.json"), JSON.stringify(result3));

let result4 = top10EconomicalBowler(deleveries, matches);
fs.writeFileSync(path.join(outputPath, "4-top-10-economic-bowler.json"), JSON.stringify(result4));

let result5 = timesEachTeamWonTossAndMatch(matches);
fs.writeFileSync(path.join(outputPath, "5-times-each-team-won-toss-and-match.json"), JSON.stringify(result5));

let result6 = playerHowWonHighestPlayerOfMatch(matches);
fs.writeFileSync(path.join(outputPath, "6-player-how-won-highest-player-of-match.json"), JSON.stringify(result6));

let result7 = strikeRateOfBatsman(deleveries, matches);
fs.writeFileSync(path.join(outputPath, "7-strikerate-of-batsman-for-eachseason.json"), JSON.stringify(result7));

let result8 = highestPlayerDismisses(deleveries);
fs.writeFileSync(path.join(outputPath, "8-highest-playes-dismissed.json"), JSON.stringify(result8));

let result9 = bestEconomyBowlerInSuperover(deleveries);
fs.writeFileSync(path.join(outputPath, "9-best-bowler-in-super-over.json"), JSON.stringify(result9));