function matchesWonPerTeamPerYear(matches) {
    if (!Array.isArray(matches) || matches === undefined) {
        return {};
    }
    let matchWonPeryear = matches.reduce((matchWonPeryear, currentMatch) => {
        if (!matchWonPeryear[currentMatch.season]) {
            matchWonPeryear[currentMatch.season] = {};
        }
        if (!matchWonPeryear[currentMatch.season][currentMatch.winner]) {
            matchWonPeryear[currentMatch.season][currentMatch.winner] = 0;
        }
        matchWonPeryear[currentMatch.season][currentMatch.winner]++;
        return matchWonPeryear;
    }, {})
    return matchWonPeryear;
}
module.exports = matchesWonPerTeamPerYear;