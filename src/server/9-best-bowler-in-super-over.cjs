function bestEconomyBowlerInSuperover(delevries) {
  if (delevries === undefined || !Array.isArray(delevries)) {
    return {};
  }
  let allBowlers = delevries.reduce((allBowlers, currOver) => {
    if (currOver.is_super_over === "1") {
      if (allBowlers.hasOwnProperty(currOver.bowler)) {
        allBowlers[currOver.bowler]["ball"] += 1;
        let run =
          Number(currOver.total_runs) -
          Number(currOver.bye_runs) -
          Number(currOver.legbye_runs);
        allBowlers[currOver.bowler]["run"] += run;
      } else {
        let run =
          Number(currOver.total_runs) -
          Number(currOver.bye_runs) -
          Number(currOver.legbye_runs);
        allBowlers[currOver.bowler] = {
          ball: 1,
          run: run,
        };
      }
    }
    return allBowlers;
  }, {});
  let ecoStats = Object.entries(allBowlers)
    .map((currentBowler) => {
      let bowlerStats = Object.entries(currentBowler[1]);
      let economy = Number(
        ((bowlerStats[1][1] * 6) / bowlerStats[0][1]).toFixed(2)
      ); // bowlerStats[1][1] contains total runs and bowlerStats[0][1] contains total ball
      return [currentBowler[0], economy]; // currentBowler[0] contains bolwers name
    })
    .sort((stat1, stat2) => stat1[1] - stat2[1]);

  return ecoStats.slice(0, 1);
}
module.exports = bestEconomyBowlerInSuperover;
