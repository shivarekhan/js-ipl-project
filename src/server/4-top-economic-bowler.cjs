function top10EconomicalBowler(deliveries, matches) {
  let filterYear = matches
    .filter((match) => match.season == 2015)
    .map((match) => parseInt(match.id));

  let allBowlers = deliveries.reduce((allBowlers, currOver) => {
    let matchId = Number(currOver.match_id);
    if (filterYear.includes(matchId)) {
      if (allBowlers.hasOwnProperty(currOver.bowler)) {
        allBowlers[currOver.bowler].balls += 1;
        allBowlers[currOver.bowler].runs += Number(currOver.total_runs);
        let run = allBowlers[currOver.bowler].runs;
        let ball = allBowlers[currOver.bowler].balls;
        allBowlers[currOver.bowler].economy = (run / (ball / 6)).toFixed(2);
      } else {
        allBowlers[currOver.bowler] = {
          runs: Number(currOver.total_runs),
          balls: 1,
          economy: 0,
        };
      }
    }
    return allBowlers;
  }, {});

  let bowlersStats = Object.entries(allBowlers).map((bowlerEntry) => {
    let bowlereconomy = Object.entries(bowlerEntry[1]);
    return [bowlerEntry[0], Number(bowlereconomy[2][1])];
  });
  bowlersStats.sort((bowler1, bowler2) => bowler1[1] - bowler2[1]);

  return bowlersStats.slice(0, 10);
}
module.exports = top10EconomicalBowler;
